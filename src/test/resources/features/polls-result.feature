Feature: Result poll

  Background:
    * url baseUrl

  @resultOK
  Scenario: Get response poll OK
    Given path 'polls','1','results'
    When method Get
    Then status 200
    And match response contains 'Not much'
    And match response contains 'The sky'
    And match response contains 'La la la'

  @resultKO
  Scenario: Get response poll KO
    Given path 'polls','2','results'
    When method Get
    Then status 404
    And match response contains 'Page not found'




